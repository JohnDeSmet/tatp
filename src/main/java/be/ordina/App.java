package be.ordina;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ){
        System.setProperty("webdriver.chrome.driver", "/Users/JoDm/Downloads/chromedriver");
        WebDriver driver = new ChromeDriver();
        driver.get("http://aqua-training.westeurope.cloudapp.azure.com:8080/training-app-0.0.1-SNAPSHOT/");
        WebElement accountBtn = driver.findElement(By.id("account-menu"));
        accountBtn.click();
        WebElement signInBtn = driver.findElement(By.id("login"));
        signInBtn.click();
        WebElement userNameInput = driver.findElement(By.id("username"));
        userNameInput.sendKeys("username");
        WebElement passwordInput = driver.findElement(By.id("password"));
        passwordInput.sendKeys("password");
        passwordInput.submit();
    }
}
