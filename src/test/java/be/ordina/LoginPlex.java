package be.ordina;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertTrue;

/**
 * Unit test for simple App.
 */
public class LoginPlex
{
    private WebDriver driver;


    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver", "/Users/JoDm/Downloads/chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.plex.tv/nl/");

    }
    @After
    public void tearDown(){
        driver.close();
    }


    @Test
    public void successfulUserLogin() throws InterruptedException {

        WebElement loginBtn = driver.findElement(By.cssSelector("[data-utm='signin-main-menu']"));
        loginBtn.click();
        //Wait for the I-frame to load (bad practice)
        Thread.sleep(3000);
        WebDriverWait wait = new WebDriverWait(driver,10);
        //switch to I-frame
        driver.switchTo().frame("fedauth-iFrame");
        WebElement emailInput = driver.findElement(By.id("email"));
        emailInput.sendKeys("christin.ritvik@ooroos.com");
        WebElement passwordInput = driver.findElement(By.id("password"));
        passwordInput.sendKeys("Test1234");
        passwordInput.submit();
        Thread.sleep(3000); //Bad practice
        WebElement userPulldown = driver.findElement(By.id("hideshow"));
        String myAccount = userPulldown.getText();
        assertTrue(myAccount.contains("Mijn account"));

    }
}
