package be.ordina;

import static org.junit.Assert.assertTrue;

import be.ordina.pages.HomePage;
import be.ordina.pages.LoginModal;
import be.ordina.utils.SeleniumUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.IOException;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    private WebDriver driver;
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver", "/Applications/chromedriver");
        driver = new ChromeDriver();
        driver.get("http://aqua-training.westeurope.cloudapp.azure.com:8080/training-app-0.0.1-SNAPSHOT/");
    }
    @After
    public void tearDown(){
        driver.close();
    }

    @Test
    public void wrongCredentials(){

    }

    @Test
    public void successfulUserLogin() throws InterruptedException {

        WebElement accountBtn = driver.findElement(By.id("account-menu"));
        accountBtn.click();
        WebElement signInBtn = driver.findElement(By.id("login"));
        signInBtn.click();
        WebElement userNameInput = driver.findElement(By.id("username"));
        userNameInput.sendKeys("username");
        WebElement passwordInput = driver.findElement(By.id("password"));
        passwordInput.sendKeys("password");
        passwordInput.submit();


        Thread.sleep(2000);
        WebElement confirmationText = driver.findElement(By.id("home-logged-message"));
        String message = confirmationText.getText();

        assertTrue(message.contains("username"));

    }
    @Test
    public void refactoredUserLogin() throws InterruptedException, IOException {
        HomePage homePage = new HomePage(driver);
        homePage.clickSignIn();

        LoginModal loginModal = new LoginModal(driver);
        loginModal.logIn("username", "password");

        String message = homePage.getSuccessMessage();
        assertTrue(message.contains("username"));
        SeleniumUtils.screenshot(driver,"screenshot");



    }
}
