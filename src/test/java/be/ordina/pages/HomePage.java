package be.ordina.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {
    private final WebDriver driver;
    private WebDriverWait wait;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver,10);
    }

    private By btnAccount = By.id("account-menu");
    private By btnLanguage = By.id("languagesnavBarDropdown");
    private By btnAccountSignIn = By.id("login");
    private By textLoginConfirmation = By.id("home-logged-message");

    public void clickSignIn(){
        driver.findElement(btnAccount).click();
        driver.findElement(btnAccountSignIn).click();
    }

    public String getSuccessMessage(){

        wait.until(ExpectedConditions.visibilityOfElementLocated(textLoginConfirmation));

        String message = driver.findElement(textLoginConfirmation).getText();
        return message;
    }


}
