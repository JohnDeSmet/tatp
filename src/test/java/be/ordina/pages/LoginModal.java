package be.ordina.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginModal {
    private final WebDriver driver;

    public LoginModal(WebDriver driver) {
        this.driver = driver;
    }

    private By tbUsername = By.id("username");
    private By tbPassword = By.id("password");

    public void logIn(String userName, String password){
        driver.findElement(tbUsername).sendKeys(userName);
        WebElement element = driver.findElement(tbPassword);
        element.sendKeys(password);
        element.submit();

    }

}
